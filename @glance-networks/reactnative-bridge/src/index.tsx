import { NativeModules, Platform } from 'react-native';
import { LogBox } from 'react-native';
LogBox.ignoreLogs(['new NativeEventEmitter()` was called with a non-null argument without the required',
  'Error: Exception in HostFunction: Malformed calls from JS: field sizes are different.'
]); // Ignore log notification by message


const LINKING_ERROR =
  `The package '@glance-networks/reactnative-bridge' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

const GlanceBridge = NativeModules.GlanceBridge
  ? NativeModules.GlanceBridge
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

//TODO: test all methods in both platforms

export function getConstants() {
  return GlanceBridge.getConstants();
}

export async function getVersion() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.getVersion((_: any, version: string) => {
        resolve(version);
       });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.getVersion();
  }
}

export function init(params: object) {
  GlanceBridge.init(params);
}

export async function addMaskedViewId(reactTag: number, label: string) {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.addMaskedViewId(reactTag, label, (_: any, version: string) => {
        resolve(version);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.addMaskedViewId(reactTag, label);
  }
}

export async function removeMaskedViewId(id: number) {
  GlanceBridge.removeMaskedViewId(id);
}

export async function getVisitorCallId() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      resolve(GlanceBridge.getVisitorCallId());
  });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.getVisitorCallId();
  }
}

export async function isInSession() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      resolve(GlanceBridge.isInSession());
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.isInSession();
  }
}

export async function isVideoAvailable() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      resolve(GlanceBridge.isVideoAvailable());
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.isVideoAvailable();
  }
}

export async function isAgentVideoEnabled() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      resolve(GlanceBridge.isAgentVideoEnabled());
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.isAgentVideoEnabled();
  }
}

export function pause(isPaused: boolean) {
  GlanceBridge.pause(isPaused);
}

export function togglePause() {
  GlanceBridge.togglePause();
}

export function release() {
  GlanceBridge.release();
}

export function updateVisitorVideoSize(width: number, height: number, stringMode: string) {
  GlanceBridge.updateVisitorVideoSize(width, height, stringMode);
}

export function updateVisitorVideoStatus(isPaused: boolean) {
  GlanceBridge.updateVisitorVideoStatus(isPaused);
}

export function updateWidgetVisibility(stringVisibility: string) {
  GlanceBridge.updateWidgetVisibility(stringVisibility);
}

export function updateWidgetLocation(stringVisibility: string) {
  GlanceBridge.updateWidgetLocation(stringVisibility);
}

export function setUserState(state: string, value: string) {
  GlanceBridge.setUserState(state, value);
}

export function sendUserMessage(messageType: string, value: string) {
  GlanceBridge.sendUserMessage(messageType, value);
}

export function startSession(startSessionParamsMap: object) {
  GlanceBridge.startSession(startSessionParamsMap);
}

export function endSession() {
  GlanceBridge.endSession();
}

export async function getVisitorId() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.getVisitorId((_: any, visitorId: string) => {
        resolve(visitorId);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.getVisitorId();
  }
}

export async function getVisitorStartParams() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.getVisitorStartParams((_: any, visitorStartParams: string) => {
        resolve(visitorStartParams);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.getVisitorStartParams();
  }
}

export function setPresenceStartParams(startSessionParamsMap: object) {
  GlanceBridge.setPresenceStartParams(startSessionParamsMap);
}

export async function getPresenceVideoMode() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.getPresenceVideoMode((_: any, videoMode: string) => {
        resolve(videoMode);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.getPresenceVideoMode();
  }
}

export async function arePresenceTermsDisplayed() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.arePresenceTermsDisplayed((_: any, arePresenceTermsDisplayed: boolean) => {
        resolve(arePresenceTermsDisplayed);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.arePresenceTermsDisplayed();
  }
}

export async function connectToPresence(presenceParamsMap: object) {
    GlanceBridge.connectToPresence(presenceParamsMap);
}

export async function sendToPresenceSession(presenceParamsMap: object) {
  GlanceBridge.sendToPresenceSession(presenceParamsMap);
}

export async function sendPresenceTermsAccepted(isAccepted: boolean) {
  GlanceBridge.sendPresenceTermsAccepted(isAccepted);
}

export async function disconnectPresence() {
  GlanceBridge.disconnectPresence();
}

export async function sendPresenceTermsDisplayed() {
  GlanceBridge.sendPresenceTermsDisplayed();
}

export async function isPresenceConnected() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.isPresenceConnected((_: any, isPresenceConnected: boolean) => {
        resolve(isPresenceConnected);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.isPresenceConnected();
  }
}

export async function getApplicationName() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.getApplicationName((_: any, applicationName: string) => {
        resolve(applicationName);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.getApplicationName();
  }
}

export async function maskKeyboard(maskKeyboard: boolean) {
  GlanceBridge.maskKeyboard(maskKeyboard);
}

export async function getCapturedScreenSize() {
  if(Platform.OS == "android") {
    return new Promise(async (resolve) => {
      GlanceBridge.getCapturedScreenSize((_: any, screenSize: object) => {
        resolve(screenSize);
      });
    });
  } else if (Platform.OS == "ios") {
     return GlanceBridge.getCapturedScreenSize();
  }
}

export async function setGlanceGroupID(groupID: string) {
  GlanceBridge.setGlanceGroupID(groupID);
}

export async function setGlanceServer(server: string) {
  GlanceBridge.setGlanceServer(server);
}
//
//  GlanceBridge.swift
//
//  Created by Felipe Melo on 08/05/24.
//

import Foundation
import React
import AVFoundation
import UIKit
import GlanceFramework

@objc(GlanceBridge)
class GlanceBridge: RCTEventEmitter {
  
  private var params = GlanceVisitorInitParams()
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  override func supportedEvents() -> [String]! {
    return [
      "GlanceEvent"
    ]
  }
  
  override func constantsToExport() -> [AnyHashable : Any]! {
    var constants =  [
      "SDK_VERSION_MAP_KEY": Constants.SDK_VERSION_MAP_KEY,
      "SESSION_KEY_MAP_KEY": Constants.SESSION_KEY_MAP_KEY,
      "MASK_KEYBOARD_MAP_KEY": Constants.MASK_KEYBOARD_MAP_KEY,
      "VIDEO_MODE_MAP_KEY": Constants.VIDEO_MODE_MAP_KEY,
      "GLANCE_TIMEOUT_MAP_KEY": Constants.GLANCE_TIMEOUT_MAP_KEY,
      "GLANCE_ATTEMPTS_MAP_KEY": Constants.GLANCE_ATTEMPTS_MAP_KEY,
      "GROUP_ID_MAP_KEY": Constants.GROUP_ID_MAP_KEY,
      "VISITOR_ID_MAP_KEY": Constants.VISITOR_ID_MAP_KEY,
      "GLANCE_SERVER_MAP_KEY": Constants.GLANCE_SERVER_MAP_KEY,
      "TOKEN_MAP_KEY": Constants.TOKEN_MAP_KEY,
      "NAME_MAP_KEY": Constants.NAME_MAP_KEY,
      "EMAIL_MAP_KEY": Constants.EMAIL_MAP_KEY,
      "PHONE_MAP_KEY": Constants.PHONE_MAP_KEY,
      "SKIP_DIALOG_MAP_KEY": Constants.SKIP_DIALOG_MAP_KEY,
      "INVOKE_SHOW_WIDGET_MAP_KEY": Constants.INVOKE_SHOW_WIDGET_MAP_KEY,
      "SITE_MAP_KEY": Constants.SITE_MAP_KEY,
      "ALLOWED_CAMERAS_MAP_KEY": Constants.ALLOWED_CAMERAS_MAP_KEY,
      "REGISTER_PRESENCE_NOTIFICATIONS_MAP_KEY": Constants.REGISTER_PRESENCE_NOTIFICATIONS_MAP_KEY,
      "SCREEN_X_VALUE": Constants.SCREEN_X_VALUE,
      "SCREEN_Y_VALUE": Constants.SCREEN_Y_VALUE,
      "DISPLAY_PARAMS_MAP_KEY": Constants.DISPLAY_PARAMS_MAP_KEY,
      "DISPLAY_PARAMS_NAME_MAP_KEY": Constants.DISPLAY_PARAMS_NAME_MAP_KEY,
      "DISPLAY_PARAMS_SCALE_MAP_KEY": Constants.DISPLAY_PARAMS_SCALE_MAP_KEY,
      "DISPLAY_PARAMS_WIDTH_MAP_KEY": Constants.DISPLAY_PARAMS_WIDTH_MAP_KEY,
      "DISPLAY_PARAMS_HEIGHT_MAP_KEY": Constants.DISPLAY_PARAMS_HEIGHT_MAP_KEY,
      "DISPLAY_PARAMS_VIDEO_MAP_KEY": Constants.DISPLAY_PARAMS_VIDEO_MAP_KEY,
      "START_PARAMS_CAPTURE_ENTIRE_SCREEN_MAP_KEY": Constants.START_PARAMS_CAPTURE_ENTIRE_SCREEN_MAP_KEY,
      "START_PARAMS_MEDIA_PROJECTION_MAP_KEY": Constants.START_PARAMS_MEDIA_PROJECTION_MAP_KEY,
      "START_PARAMS_MAIN_CALL_ID_MAP_KEY": Constants.START_PARAMS_MAIN_CALL_ID_MAP_KEY,
      "START_PARAMS_MAX_GUESTS_MAP_KEY": Constants.START_PARAMS_MAX_GUESTS_MAP_KEY,
      "START_PARAMS_SHOW_MAP_KEY": Constants.START_PARAMS_SHOW_MAP_KEY,
      "START_PARAMS_GUEST_INFO_FLAGS_MAP_KEY": Constants.START_PARAMS_GUEST_INFO_FLAGS_MAP_KEY,
      "START_PARAMS_ENCRYPT_MAP_KEY": Constants.START_PARAMS_ENCRYPT_MAP_KEY,
      "START_PARAMS_REQUEST_RC_MAP_KEY": Constants.START_PARAMS_REQUEST_RC_MAP_KEY,
      "START_PARAMS_INSTANT_JOIN_MAP_KEY": Constants.START_PARAMS_INSTANT_JOIN_MAP_KEY,
      "START_PARAMS_FORCE_TUNNEL_MAP_KEY": Constants.START_PARAMS_FORCE_TUNNEL_MAP_KEY,
      "START_PARAMS_VIEWER_CLOSEABLE_MAP_KEY": Constants.START_PARAMS_VIEWER_CLOSEABLE_MAP_KEY,
      "START_PARAMS_REPORT_ERRORS_MAP_KEY": Constants.START_PARAMS_REPORT_ERRORS_MAP_KEY,
      "START_PARAMS_PERSIST_MAP_KEY": Constants.START_PARAMS_PERSIST_MAP_KEY,
      "START_PARAMS_SHOW_TERMS_MAP_KEY": Constants.START_PARAMS_SHOW_TERMS_MAP_KEY,
      "START_PARAMS_PRESENCE_START_MAP_KEY": Constants.START_PARAMS_PRESENCE_START_MAP_KEY,
      "START_PARAMS_PAUSED_MAP_KEY": Constants.START_PARAMS_PAUSED_MAP_KEY,
      "START_PARAMS_TERMS_URL_MAP_KEY": Constants.START_PARAMS_TERMS_URL_MAP_KEY,
      "GLANCE_EVENT_LISTENER_KEY": Constants.GLANCE_EVENT_LISTENER_KEY,
      "GUEST_COUNT_MAP_KEY": Constants.GUEST_COUNT_MAP_KEY,
      "EVENT_RECEIVED_MESSAGE_MAP_KEY": Constants.EVENT_RECEIVED_MESSAGE_MAP_KEY,
      "EVENT_RECEIVED_MESSAGE_VALUE_MAP_KEY": Constants.EVENT_RECEIVED_MESSAGE_VALUE_MAP_KEY,
      "EVENT_WARNING_MESSAGE_MAP_KEY": Constants.EVENT_WARNING_MESSAGE_MAP_KEY,
      "EVENT_CODE_MAP_KEY": Constants.EVENT_CODE_MAP_KEY,
      "EVENT_TYPE_MAP_KEY": Constants.EVENT_TYPE_MAP_KEY,
      "PRESENCE_EVENT_MAP_KEY": Constants.PRESENCE_EVENT_MAP_KEY,
      "PRESENCE_DATA_MAP_KEY": Constants.PRESENCE_DATA_MAP_KEY
    ]
    
    Constants.EVENTS.forEach { event in
      constants[event] = event
    }
    
    Constants.VIDEO_MODES.forEach { videoMode in
      constants[videoMode] = videoMode
    }
    
    return constants
  }
  
  @objc func getVersion(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
    print("getVersion")
    resolve("6.8.8")
  }
  
  @objc func `init`(_ properties: [String: Any]) {
    Glance.setDelegate(self)
    
    params.groupid = properties[Constants.GROUP_ID_MAP_KEY] as? Int32 ?? 0
    params.visitorid = properties[Constants.VISITOR_ID_MAP_KEY] as? String
    params.video = properties["video"] as? Bool ?? false
    Glance.initVisitor(params: params)
  }
  
  @objc func startSession(_ properties: [String: Any]) {
    guard let params = GlanceStartParams() else { return }
    params.key = properties["key"] as? String ?? "GLANCE_KEYTYPE_RANDOM"
    Glance.startSession(params: params)
  }
  
  @objc func endSession() {
    Glance.endSession()
  }
  
  @objc func addMaskedViewId(_ reactTag: NSNumber, label: String) {
    DispatchQueue.main.async {
      if let view = self.bridge.uiManager.view(forReactTag: reactTag) {
        Glance.addMaskedView(view, withLabel: label)
      }
    }
  }
  
  @objc func removeMaskedViewId(_ reactTag: NSNumber) {
    DispatchQueue.main.async {
      if let view = self.bridge.uiManager.view(forReactTag: reactTag) {
        Glance.removeMaskedView(view)
      }
    }
  }
  
  @objc func getVisitorCallId(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
    print("getVisitorCallId")
    resolve(Glance.getVisitorCallId())
  }
  
  @objc func isInSession(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
    print("isInSession")
    resolve(Glance.isInSession())
  }
  
  @objc func isVideoAvailable(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
    print("isVideoAvailable")
    resolve(Glance.isVideoAvailable())
  }
  
  @objc func isAgentVideoEnabled(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
    print("isAgentVideoEnabled")
    resolve(Glance.isAgentVideoEnabled())
  }
  
  @objc func pause(_ isPaused: Bool) {
    print("pause")
    Glance.pause(isPaused)
  }
  
  @objc func togglePause() {
    print("togglePause")
    Glance.togglePause()
  }
  
  @objc func updateVisitorVideoSize(_ width: Int, height: Int, mode: String) {
    print("updateVisitorVideoSize")
    Glance.updateVisitorVideoSize(mode: mode, width: width, height: height)
  }
  
  @objc func updateWidgetVisibility(_ mode: String) {
    print("updateWidgetVisibility")
    Glance.updateWidgetVisibility(mode)
  }
  
  @objc func updateWidgetLocation(_ corner: String) {
    print("updateWidgetLocation")
    Glance.updateWidgetLocation(corner)
  }
  
  @objc func sendUserMessage(_ messageType: String, value: String) {
    print("sendUserMessage")
    Glance.sendUserMessage(messageType, value: value)
  }
  
  @objc func getVisitorId(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
    print("getVisitorId")
    resolve(Glance.getVisitorId())
  }
  
  @objc func maskKeyboard(_ mask: Bool) {
    print("maskKeyboard")
    Glance.maskKeyboard(mask)
  }
  
}

extension GlanceBridge: GlanceDelegate {
  
  func onGlanceEvent(_ event: GlanceEvent) {
    var properties = event.properties
    properties["code"] = event.codeAsString
    properties["message"] = event.message
    properties["type"] = event.type
    
    sendEvent(withName: "GlanceEvent", body: properties)
  }
  
  func sessionConnected(dismissAction: (() -> Void)?) {
    
  }
  
  func presenceConnected(dismissAction: (() -> Void)?) {
    
  }
  
  func receivedSessionCode(_ sessionCode: String) {
    
  }
  
  func sessionAgentConnected() {
    
  }
  
  public func onGuestCountChange(guests: [GlanceAgent], response: String?) {
    
  }
  
  func sessionEnded() {
    
  }
  
  func receivedVideoCaptureImage(_ image: UIImage) {
    
  }
  
  func didStartVideoCapture(_ captureSession: AVCaptureSession) {
    
  }
  
  func didStopVideoCapture(_ captureSession: AVCaptureSession) {
    
  }
  
  func didStartPreviewVideoCapture(_ captureSession: AVCaptureSession) {
    
  }
  
  func didStopPreviewVideoCapture(_ captureSession: AVCaptureSession) {
    
  }
  
  func didPauseVisitorVideo() {
    
  }
  
  func didStartAgentViewer(_ glanceAgentViewer: UIView) {
    
  }
  
  func didStopAgentViewer() {
    
  }
  
  func onUpdateVisibility(to visibility: GlanceFramework.Visibility) {
    
  }
  
  func onUpdateLocation(to location: GlanceFramework.Location) {
    
  }
  
  func onUpdateSize(size: String) {
    
  }
  
  func askForVideoSession(confirmAction: @escaping (() -> Void), cancelAction: (() -> Void)?) {
    confirmAction() // Automatically confirm for now
  }
  
  func agentAskedForVideoSession() {
    
  }
  
  func onUpdateAgentVideo() {
    
  }
  
  func initTimeoutExpired() {
    
  }
  
  func startTimeoutExpired() {
    
  }
  
  func presenceTimeoutExpired() {
    
  }
  
  func onError(_ error: GlanceFramework.GlanceError) {
    
  }
}

public class Constants {
  
  // MISCELANEOUS
  public static let SDK_VERSION_MAP_KEY = "version";
  
  // SDK SETTINGS
  public static let SESSION_KEY_MAP_KEY = "sessionkey";
  
  public static let MASK_KEYBOARD_MAP_KEY = "maskKeyboard";
  
  public static let VIDEO_MODE_MAP_KEY = "videoMode";
  
  public static let GLANCE_TIMEOUT_MAP_KEY = "timeout";
  
  public static let GLANCE_ATTEMPTS_MAP_KEY = "attempts";
  
  public static let GROUP_ID_MAP_KEY = "groupID";
  
  public static let VISITOR_ID_MAP_KEY = "visitorID";
  
  public static let GLANCE_SERVER_MAP_KEY = "glanceServer";
  
  public static let TOKEN_MAP_KEY = "token";
  
  public static let NAME_MAP_KEY = "name";
  
  public static let EMAIL_MAP_KEY = "email";
  
  public static let PHONE_MAP_KEY = "phone";
  
  public static let SKIP_DIALOG_MAP_KEY = "skipDialog";
  
  public static let INVOKE_SHOW_WIDGET_MAP_KEY = "invokeShowWidget";
  
  public static let SITE_MAP_KEY = "site";
  
  public static let ALLOWED_CAMERAS_MAP_KEY = "allowedCameras";
  
  public static let REGISTER_PRESENCE_NOTIFICATIONS_MAP_KEY = "registerNotifications";
  
  public static let SCREEN_X_VALUE = "screenX";
  
  public static let SCREEN_Y_VALUE = "screenY";
  
  // START PARAMS
  public static let DISPLAY_PARAMS_MAP_KEY = "displayParams";
  
  public static let DISPLAY_PARAMS_NAME_MAP_KEY = "displayName";
  
  public static let DISPLAY_PARAMS_SCALE_MAP_KEY = "displayScale";
  
  public static let DISPLAY_PARAMS_WIDTH_MAP_KEY = "displayCaptureWidth";
  
  public static let DISPLAY_PARAMS_HEIGHT_MAP_KEY = "displayCaptureHeight";
  
  public static let DISPLAY_PARAMS_VIDEO_MAP_KEY = "displayVideo";
  
  public static let START_PARAMS_CAPTURE_ENTIRE_SCREEN_MAP_KEY = "captureEntireScreen";
  
  public static let START_PARAMS_MEDIA_PROJECTION_MAP_KEY = "mediaProjectionEnabled";
  
  public static let START_PARAMS_MAIN_CALL_ID_MAP_KEY = "mainCallId";
  
  public static let START_PARAMS_MAX_GUESTS_MAP_KEY = "maxGuests";
  
  public static let START_PARAMS_SHOW_MAP_KEY = "show";
  
  public static let START_PARAMS_GUEST_INFO_FLAGS_MAP_KEY = "guestInfoFlags";
  
  public static let START_PARAMS_ENCRYPT_MAP_KEY = "encrypt";
  
  public static let START_PARAMS_REQUEST_RC_MAP_KEY = "requestRC";
  
  public static let START_PARAMS_INSTANT_JOIN_MAP_KEY = "instantJoin";
  
  public static let START_PARAMS_FORCE_TUNNEL_MAP_KEY = "forceTunnel";
  
  public static let START_PARAMS_VIEWER_CLOSEABLE_MAP_KEY = "viewerCloseable";
  
  public static let START_PARAMS_REPORT_ERRORS_MAP_KEY = "reportErrors";
  
  public static let START_PARAMS_PERSIST_MAP_KEY = "persist";
  
  public static let START_PARAMS_SHOW_TERMS_MAP_KEY = "showTerms";
  
  public static let START_PARAMS_PRESENCE_START_MAP_KEY = "presenceStart";
  
  public static let START_PARAMS_PAUSED_MAP_KEY = "paused";
  
  public static let START_PARAMS_TERMS_URL_MAP_KEY = "termsUrl";
  
  
  // EVENTS
  public static let GLANCE_EVENT_LISTENER_KEY = "GlanceEvent";
  
  public static let GUEST_COUNT_MAP_KEY = "guestCount";
  
  public static let EVENT_RECEIVED_MESSAGE_MAP_KEY = "receivedMessage";
  
  public static let EVENT_RECEIVED_MESSAGE_VALUE_MAP_KEY = "receivedMessageValue";
  
  public static let EVENT_WARNING_MESSAGE_MAP_KEY = "warningMsg";
  
  public static let EVENT_CODE_MAP_KEY = "code";
  
  public static let EVENT_TYPE_MAP_KEY = "type";
  
  // PRESENCE
  public static let PRESENCE_EVENT_MAP_KEY = "presenceEvent";
  
  public static let PRESENCE_DATA_MAP_KEY = "presenceDataMap";
  
  // EVENTS
  public static let EVENTS = [
    "EventNone",
    "EventInvalidParameter",
    "EventInvalidState",
    "EventLoginSucceeded",
    "EventLoginFailed",
    "EventPrivilegeViolation",
    "EventInvalidWebserver",
    "EventUpgradeAvailable",
    "EventUpgradeRequired",
    "EventCompositionDisabled",
    "EventConnectedToSession",
    "EventSwitchingToView",
    "EventStartSessionFailed",
    "EventJoinFailed",
    "EventSessionEnded",
    "EventFirstGuestSession",
    "EventTunneling",
    "EventRestartRequired",
    "EventConnectionWarning",
    "EventClearWarning",
    "EventGuestCountChange",
    "EventViewerClose",
    "EventActionsChange",
    "EventDriverInstallError",
    "EventRCDisabled",
    "EventDeviceDisconnected",
    "EventDeviceReconnected",
    "EventGesture",
    "EventException",
    "EventScreenshareInvitation",
    "EventMessageReceived",
    "EventChildSessionStarted",
    "EventChildSessionEnded",
    "EventJoinChildSessionFailed",
    "EventVisitorInitialized",
    "EventPresenceConnected",
    "EventPresenceConnectFail",
    "EventPresenceShowTerms",
    "EventPresenceStartSession",
    "EventPresenceSignal",
    "EventPresenceBlur",
    "EventPresenceSendFail",
    "EventPresenceNotConfigured",
    "EventPresenceDisconnected",
    "EventPresenceStartVideo",
    "EventVisitorVideoRequested"
  ]
  
  // VIDEO MODES
  public static let VIDEO_MODES = [
    "VideoOff",
    "VideoSmallVisitor",
    "VideoLargeVisitor",
    "VideoSmallMultiway",
    "VideoLargeMultiway"
  ]
  
}

extension GlanceEvent {
  var codeAsString: String {
    switch self.code {
    case EventNone: "EventNone"
    case EventInvalidParameter: "EventInvalidParameter"
    case EventInvalidState: "EventInvalidState"
    case EventLoginSucceeded: "EventLoginSucceeded"
    case EventLoginFailed: "EventLoginFailed"
    case EventPrivilegeViolation: "EventPrivilegeViolation"
    case EventInvalidWebserver: "EventInvalidWebserver"
    case EventUpgradeAvailable: "EventUpgradeAvailable"
    case EventUpgradeRequired: "EventUpgradeRequired"
    case EventCompositionDisabled: "EventCompositionDisabled"
    case EventConnectedToSession: "EventConnectedToSession"
    case EventSwitchingToView: "EventSwitchingToView"
    case EventStartSessionFailed: "EventStartSessionFailed"
    case EventJoinFailed: "EventJoinFailed"
    case EventSessionEnded: "EventSessionEnded"
    case EventFirstGuestSession: "EventFirstGuestSession"
    case EventTunneling: "EventTunneling"
    case EventRestartRequired: "EventRestartRequired"
    case EventConnectionWarning: "EventConnectionWarning"
    case EventClearWarning: "EventClearWarning"
    case EventGuestCountChange: "EventGuestCountChange"
    case EventViewerClose: "EventViewerClose"
    case EventActionsChange: "EventActionsChange"
    case EventDriverInstallError: "EventDriverInstallError"
    case EventRCDisabled: "EventRCDisabled"
    case EventDeviceDisconnected: "EventDeviceDisconnected"
    case EventDeviceReconnected: "EventDeviceReconnected"
    case EventGesture: "EventGesture"
    case EventException: "EventException"
    case EventScreenshareInvitation: "EventScreenshareInvitation"
    case EventMessageReceived: "EventMessageReceived"
    case EventChildSessionStarted: "EventChildSessionStarted"
    case EventChildSessionEnded: "EventChildSessionEnded"
    case EventJoinChildSessionFailed: "EventJoinChildSessionFailed"
    case EventVisitorInitialized: "EventVisitorInitialized"
    case EventPresenceConnected: "EventPresenceConnected"
    case EventPresenceConnectFail: "EventPresenceConnectFail"
    case EventPresenceShowTerms: "EventPresenceShowTerms"
    case EventPresenceStartSession: "EventPresenceStartSession"
    case EventPresenceSignal: "EventPresenceSignal"
    case EventPresenceBlur: "EventPresenceBlur"
    case EventPresenceSendFail: "EventPresenceSendFail"
    case EventPresenceNotConfigured: "EventPresenceNotConfigured"
    case EventPresenceDisconnected: "EventPresenceDisconnected"
    case EventPresenceStartVideo: "EventPresenceStartVideo"
    case EventVisitorVideoRequested: "EventVisitorVideoRequested"
    default: ""
    }
  }
}

extension GlanceVideoMode {
  var asString: String {
    switch self {
    case VideoOff: "VideoOff"
    case VideoSmallVisitor: "VideoSmallVisitor"
    case VideoLargeVisitor: "VideoLargeVisitor"
    case VideoSmallMultiway: "VideoSmallMultiway"
    case VideoLargeMultiway: "VideoLargeMultiway"
    default: ""
    }
  }
}

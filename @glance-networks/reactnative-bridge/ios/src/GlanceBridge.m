//
//  GlanceBridge.m
//
//  Created by Felipe Melo on 08/05/24.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(GlanceBridge, RCTEventEmitter)

RCT_EXTERN_METHOD(getVersion:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(init:(NSDictionary)properties)
RCT_EXTERN_METHOD(startSession:(NSDictionary)properties)
RCT_EXTERN_METHOD(endSession)
RCT_EXTERN_METHOD(addMaskedViewId:(nonnull NSNumber *)reactTag label:(NSString *)label)
RCT_EXTERN_METHOD(removeMaskedViewId:(nonnull NSNumber *)reactTag)
RCT_EXTERN_METHOD(getVisitorCallId:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(isInSession:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(isVideoAvailable:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(isAgentVideoEnabled:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(pause:(BOOL)isPaused)
RCT_EXTERN_METHOD(togglePause)
RCT_EXTERN_METHOD(updateVisitorVideoSize:(NSInteger)width height:(NSInteger)height mode:(NSString *)mode)
RCT_EXTERN_METHOD(updateWidgetVisibility:(NSString *)mode)
RCT_EXTERN_METHOD(updateWidgetLocation:(NSString *)corner)
RCT_EXTERN_METHOD(sendUserMessage:(NSString *)messageType value:(NSString *)value)
RCT_EXTERN_METHOD(getVisitorId:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(maskKeyboard:(BOOL)mask)

@end

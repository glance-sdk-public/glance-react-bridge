// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.9.2 (swiftlang-5.9.2.2.56 clang-1500.1.0.2.5)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name GlanceFramework
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import Combine
import DeveloperToolsSupport
import Foundation
import GlanceCore
@_exported import GlanceFramework
import Swift
import SwiftUI
import UIKit
import WebKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
@objc public protocol GlanceServiceType {
  @objc var termsUrl: Swift.String { get }
  @objc func setVisitorParams(_ params: GlanceVisitorInitParams)
  @objc func setPresence(_ isPresenceOn: Swift.Bool, maxConnectAttempts: Swift.Int32)
  @objc func startSession(params: GlanceStartParams, maxConnectAttempts: Swift.Int)
  @objc func endSession()
  @objc func startVideoPreview()
  @objc func stopVideoPreview()
  @objc func addVideo()
  @objc func send(message: Swift.String, properties: Swift.String)
}
@objc final public class GlanceService : ObjectiveC.NSObject, GlanceFramework.GlanceServiceType {
  @objc final public var termsUrl: Swift.String
  public init(termsUrl: Swift.String = "https://ww2.glance.net/")
  @objc final public func setVisitorParams(_ params: GlanceVisitorInitParams)
  @objc final public func setPresence(_ isPresenceOn: Swift.Bool, maxConnectAttempts: Swift.Int32)
  @objc final public func startSession(params: GlanceStartParams, maxConnectAttempts: Swift.Int)
  @objc final public func endSession()
  @objc final public func startVideoPreview()
  @objc final public func stopVideoPreview()
  @objc final public func send(message: Swift.String, properties: Swift.String)
  @objc final public func addVideo()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class GlanceAgent : ObjectiveC.NSObject, Swift.Decodable {
  public var agentRole: Swift.String?
  public var name: Swift.String?
  public var partnerId: Swift.Int?
  public var partnerUserId: Swift.String?
  public var role: Swift.String?
  public var title: Swift.String?
  public var username: Swift.String?
  @objc override dynamic public init()
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
public protocol GlanceUIDelegate {
  func onError(_ error: GlanceFramework.GlanceError)
}
@_hasMissingDesignatedInitializers final public class GlanceDefaultUI {
  public static func setVisitorParams(_ params: GlanceVisitorInitParams, maxConnectAttempts: Swift.Int = 0)
  public static func start(key: Swift.String? = nil, videoMode: GlanceVideoMode)
  public static func endSession()
  public static func setPresence(_ isPresenceOn: Swift.Bool, maxConnectAttempts: Swift.Int32? = nil)
  public static func setDelegate(_ delegate: any GlanceFramework.GlanceUIDelegate)
  @objc deinit
}
extension GlanceFramework.GlanceDefaultUI : GlanceFramework.GlanceDelegate {
  @objc final public func sessionConnected(dismissAction: (() -> Swift.Void)?)
  @objc final public func presenceConnected(dismissAction: (() -> Swift.Void)?)
  @objc final public func receivedSessionCode(_ sessionCode: Swift.String)
  @objc final public func sessionEnded()
  @objc final public func sessionAgentConnected()
  @objc final public func onGuestCountChange(guests: [GlanceFramework.GlanceAgent], response: Swift.String?)
  @objc final public func receivedVideoCaptureImage(_ image: UIKit.UIImage)
  @objc final public func didStartVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didStopVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didStartPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didStopPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didPauseVisitorVideo()
  @objc final public func didStartAgentViewer(_ glanceAgentViewer: UIKit.UIView)
  @objc final public func didStopAgentViewer()
  @objc final public func onUpdateVisibility(to visibility: GlanceFramework.Visibility)
  @objc final public func onUpdateLocation(to location: GlanceFramework.Location)
  @objc final public func onUpdateSize(size: Swift.String)
  @objc final public func askForVideoSession(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
  @objc final public func agentAskedForVideoSession()
  @objc final public func onUpdateAgentVideo()
  @objc final public func initTimeoutExpired()
  @objc final public func startTimeoutExpired()
  @objc final public func presenceTimeoutExpired()
  @objc final public func onGlanceEvent(_ event: GlanceEvent)
  @objc final public func onError(_ error: GlanceFramework.GlanceError)
}
@objc final public class DialogsCoordinator : ObjectiveC.NSObject {
  @objc final public let service: any GlanceFramework.GlanceServiceType
  @objc public init(service: any GlanceFramework.GlanceServiceType = GlanceService())
  @objc final public func presentAppShareDialog()
  @objc final public func presentAppShareVideoDialog()
  @objc final public func presentAgentAskShareVideoDialog()
  @objc final public func presentWaitingAgentDialog(dismissAction: (() -> Swift.Void)?)
  @objc final public func presentConfirmationDialog(confirmAction: (() -> Swift.Void)? = nil)
  @objc final public func presentShowCodeDialog(code: Swift.String)
  @objc final public func presentStartDialog(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
  @objc deinit
}
@available(iOS 13.0, *)
extension SwiftUI.Image {
  public enum GlanceImages : Swift.String {
    case glanceAgentCursor
    case glanceClose
    case glanceConnecting
    case glanceDraw
    case glanceFlipCamera
    case glanceHangup
    case glanceHeadset
    case glanceHeadsetMan
    case glanceKeypad
    case glanceLogo
    case glanceMicrophone
    case glanceMicrophoneOff
    case glanceNewBlurOff
    case glanceNewBlurOn
    case glanceNewCamera
    case glanceCaratDown
    case glanceNewCaratUp
    case glanceNewClose
    case glanceNewDivider
    case glanceNewFlipCamera
    case glanceNewHangup
    case glanceNewHeadphones
    case glanceNewLogo
    case glanceNewMaximize
    case glanceNewMicrophoneOff
    case glanceNewMicrophoneOn
    case glanceNewMinimize
    case glanceNewSettings
    case glanceNewSpeakerphoneOff
    case glanceNewSpeakerphoneOn
    case glanceNewVideoFullscreenMinimizeDarkDark
    case glanceNewVideo_Fullscreen_Minimize_Dark
    case glanceNewVideo_Fullscreen
    case glanceNewVideoOff
    case glanceNewVideoOn
    case glanceNewVisitor
    case glancePhone
    case glanceSpeakerphone
    case glanceSpeakerphoneOff
    case glanceTabArrow
    case glanceVideo
    case glanceVideoDialogClose
    case glanceVideoInfoImage
    case noImage
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public init(glanceImage: SwiftUI.Image.GlanceImages)
  public func resizableFrame(width: CoreFoundation.CGFloat, height: CoreFoundation.CGFloat) -> some SwiftUI.View
  
}
public enum SessionType {
  case screenshare, twoWayVideo, oneWayVideo, fullScreen
  public static func == (a: GlanceFramework.SessionType, b: GlanceFramework.SessionType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum EventCode : Swift.Int {
  case EventNone
  case EventInvalidParameter
  case EventInvalidState
  case EventLoginSucceeded
  case EventLoginFailed
  case EventPrivilegeViolation
  case EventInvalidWebserver
  case EventUpgradeAvailable
  case EventUpgradeRequired
  case EventCompositionDisabled
  case EventConnectedToSession
  case EventSwitchingToView
  case EventStartSessionFailed
  case EventJoinFailed
  case EventSessionEnded
  case EventFirstGuestSession
  case EventTunneling
  case EventRestartRequired
  case EventConnectionWarning
  case EventClearWarning
  case EventGuestCountChange
  case EventViewerClose
  case EventActionsChange
  case EventDriverInstallError
  case EventRCDisabled
  case EventDeviceDisconnected
  case EventDeviceReconnected
  case EventGesture
  case EventException
  case EventScreenshareInvitation
  case EventMessageReceived
  case EventChildSessionStarted
  case EventChildSessionEnded
  case EventJoinChildSessionFailed
  case EventVisitorInitialized
  case EventPresenceConnected
  case EventPresenceConnectFail
  case EventPresenceShowTerms
  case EventPresenceStartSession
  case EventPresenceSignal
  case EventPresenceBlur
  case EventPresenceSendFail
  case EventPresenceNotConfigured
  case EventPresenceDisconnected
  case EventPresenceStartVideo
  case EventVisitorVideoRequested
  public init(fromRawValue: Swift.UInt)
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum EventType : Swift.Int {
  case none
  case info
  case warning
  case error
  case assertFail
  public init(fromRawValue: Swift.UInt)
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public struct GlanceSDKEvent {
  public var code: GlanceFramework.EventCode
  public var type: GlanceFramework.EventType
  public var message: Swift.String
  public var properties: [Swift.AnyHashable : Any]
  public init(code: GlanceFramework.EventCode, type: GlanceFramework.EventType, message: Swift.String, properties: [Swift.AnyHashable : Any])
  public init(_ object: GlanceEvent)
}
extension SwiftUI.View {
  public func addMaskedView(withLabel label: Swift.String) -> some SwiftUI.View
  
}
@objc public class GlanceError : ObjectiveC.NSObject {
  final public let title: Swift.String
  final public let message: Swift.String
  public init(title: Swift.String, message: Swift.String)
  @objc deinit
}
@objc public enum Visibility : ObjectiveC.NSInteger {
  case tab = 1
  case full = 2
  public init?(rawValue: ObjectiveC.NSInteger)
  public typealias RawValue = ObjectiveC.NSInteger
  public var rawValue: ObjectiveC.NSInteger {
    get
  }
}
@objc public enum Location : ObjectiveC.NSInteger {
  case topLeft = 1
  case topRight = 2
  case bottomLeft = 3
  case bottomRight = 4
  public init?(rawValue: ObjectiveC.NSInteger)
  public typealias RawValue = ObjectiveC.NSInteger
  public var rawValue: ObjectiveC.NSInteger {
    get
  }
}
@objc public protocol GlanceDelegate {
  @objc func sessionConnected(dismissAction: (() -> Swift.Void)?)
  @objc func presenceConnected(dismissAction: (() -> Swift.Void)?)
  @objc func receivedSessionCode(_ sessionCode: Swift.String)
  @objc func sessionAgentConnected()
  @objc func onGuestCountChange(guests: [GlanceFramework.GlanceAgent], response: Swift.String?)
  @objc func sessionEnded()
  @objc func receivedVideoCaptureImage(_ image: UIKit.UIImage)
  @objc func didStartVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didStopVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didStartPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didStopPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didPauseVisitorVideo()
  @objc func didStartAgentViewer(_ glanceAgentViewer: UIKit.UIView)
  @objc func didStopAgentViewer()
  @objc func onUpdateVisibility(to visibility: GlanceFramework.Visibility)
  @objc func onUpdateLocation(to location: GlanceFramework.Location)
  @objc func onUpdateSize(size: Swift.String)
  @objc func askForVideoSession(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
  @objc func agentAskedForVideoSession()
  @objc func onUpdateAgentVideo()
  @objc func initTimeoutExpired()
  @objc func startTimeoutExpired()
  @objc func presenceTimeoutExpired()
  @objc func onGlanceEvent(_ event: GlanceEvent)
  @objc func onError(_ error: GlanceFramework.GlanceError)
}
@_inheritsConvenienceInitializers @objc final public class Glance : ObjectiveC.NSObject {
  final public var videoSession: GlanceVideoSession?
  final public var previewCameraManager: SessionUICameraManager?
  final public var messageManager: GlanceFramework.MessageManager
  @objc weak final public var delegate: (any GlanceFramework.GlanceDelegate)?
  @objc public static var shared: GlanceFramework.Glance
  @objc final public var initParams: GlanceVisitorInitParams?
  @objc final public var startParams: GlanceStartParams?
  @objc override dynamic public init()
  public static func initVisitor(params: GlanceVisitorInitParams, timeout: Swift.Double? = nil)
  public static func startSession(params: GlanceStartParams, timeout: Swift.Double? = nil, maxConnectAttempts: Swift.Int? = nil)
  public static func getVisitorCallId() -> Swift.Int
  public static func isInSession() -> Swift.Bool
  public static func isVideoAvailable() -> Swift.Bool
  public static func isAgentVideoEnabled() -> Swift.Bool
  public static func pause(_ isPaused: Swift.Bool)
  public static func togglePause()
  public static func startVideoPreview()
  public static func updateVisitorVideoSize(mode: Swift.String, width: Swift.Int = 95, height: Swift.Int = 95)
  public static func updateWidgetVisibility(_ mode: Swift.String)
  public static func updateWidgetLocation(_ corner: Swift.String)
  public static func sendUserMessage(_ messageType: Swift.String, value: Swift.String)
  public static func getVisitorId() -> Swift.String?
  public static func stopVideoPreview()
  public static func startVideoSession()
  public static func setDelegate(_ delegate: any GlanceFramework.GlanceDelegate)
  public static func endSession()
  public static func setPresence(_ isPresenceOn: Swift.Bool, timeout: Swift.Double? = nil, maxConnectAttempts: Swift.Int32? = nil)
  public static func send(message: Swift.String, properties: Swift.String)
  public static func maskKeyboard(_ mask: Swift.Bool)
  public static func addMaskedView(_ view: UIKit.UIView, withLabel: Swift.String)
  public static func removeMaskedView(_ view: UIKit.UIView)
  @objc final public func startCaptureSession()
  @objc final public func stopCaptureSession()
  @objc deinit
}
extension GlanceFramework.Glance : GlanceVisitorDelegate {
  @objc final public func glanceVisitorEvent(_ event: GlanceEvent)
}
extension GlanceFramework.Glance : GlanceVideoSessionDelegate {
  @objc final public func glanceVideoSessionDidStart(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidStartVideoCapture(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidConnectVideoSource(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidDisconnectVideoSource(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidFailToConnectVideoSource(_ session: GlanceVideoSession!, error: (any Swift.Error)!)
  @objc final public func sessionUICameraManagerDidCapture(_ image: UIKit.UIImage!)
  @objc final public func glanceVideoSessionDidConnectStreamer(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidDisconnnectStreamer(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionWillStartStreaming(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionWillStopStreaming(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidFailConnectStreamer(_ session: GlanceVideoSession!, error: (any Swift.Error)!)
  @objc final public func glanceVideoSessionWillChangeQuality(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidEnd(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionInvitation(_ session: GlanceVideoSession!, sessiontype: Swift.String!, username: Swift.String!, sessionkey sesionkey: Swift.String!)
}
extension GlanceFramework.Glance : SessionUICameraManagerDelegate {
  @objc final public func sessionUICameraManagerDidStartVideoCapture(_ instance: SessionUICameraManager)
  @objc final public func sessionUICameraManagerDidStopVideoCapture(_ instance: SessionUICameraManager)
}
extension GlanceFramework.Glance : SessionUIDelegate {
  @objc final public func sessionUIVoiceAuthenticationRequired()
  @objc final public func sessionUIVoiceAuthenticationFailed()
  @objc final public func sessionUIDidError(_ error: (any Swift.Error)!)
  @objc final public func sessionUIDialogAccepted()
  @objc final public func sessionUIDialogCancelled()
}
extension GlanceFramework.Glance : GlanceCustomViewerDelegate {
  @objc final public func glanceViewerDidStart(_ glanceView: UIKit.UIView, size: CoreFoundation.CGSize)
  @objc final public func glanceViewerDidStop(_ glanceView: UIKit.UIView)
  @objc final public func glanceViewerIsStopping(_ glanceView: UIKit.UIView)
  @objc final public func glanceViewerIsStarting(_ glanceView: UIKit.UIView)
}
@_Concurrency.MainActor(unsafe) public struct SessionAppShareDialogView_Previews : SwiftUI.PreviewProvider {
  @_Concurrency.MainActor(unsafe) public static var previews: some SwiftUI.View {
    get
  }
  public typealias Previews = @_opaqueReturnTypeOf("$s15GlanceFramework34SessionAppShareDialogView_PreviewsV8previewsQrvpZ", 0) __
}
extension SwiftUI.View {
  public func cornerRadius(_ radius: CoreFoundation.CGFloat, corners: UIKit.UIRectCorner) -> some SwiftUI.View
  
}
public enum Message : Swift.String {
  case visitorPaused
  case widgetLocation
  case widgetVisibility
  case videoSize
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public protocol MessageManagerType {
  func pause(_ isPaused: Swift.Bool)
  func updateWidgetLocation(_ location: Swift.String)
  func updateWidgetVisibility(_ visibility: Swift.String)
  func updateVisitorVideoSize(_ size: GlanceFramework.VideoSizeMessage, width: Swift.Int, height: Swift.Int)
}
public enum VideoSizeMessage : Swift.String {
  case large
  case small
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
final public class MessageManager : GlanceFramework.MessageManagerType {
  public init()
  final public func pause(_ isPaused: Swift.Bool)
  final public func updateWidgetLocation(_ location: Swift.String)
  final public func updateWidgetVisibility(_ visibility: Swift.String)
  final public func updateVisitorVideoSize(_ size: GlanceFramework.VideoSizeMessage, width: Swift.Int = 95, height: Swift.Int = 95)
  @objc deinit
}
@objc final public class SessionCoordinator : ObjectiveC.NSObject {
  public init(dialogsCoordinator: GlanceFramework.DialogsCoordinator, service: any GlanceFramework.GlanceServiceType, messageManager: any GlanceFramework.MessageManagerType)
  final public func start(sessionType: GlanceFramework.SessionType = .screenshare)
  @objc deinit
}
@available(iOS 13.0, *)
public struct StartDialogButtonsView : SwiftUI.View {
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s15GlanceFramework22StartDialogButtonsViewV4bodyQrvp", 0) __
}
extension GlanceFramework.Location : Swift.Equatable {}
extension GlanceFramework.Location : Swift.Hashable {}
extension GlanceFramework.Location : Swift.RawRepresentable {}
@available(iOS 13.0, *)
extension SwiftUI.Image.GlanceImages : Swift.Equatable {}
@available(iOS 13.0, *)
extension SwiftUI.Image.GlanceImages : Swift.Hashable {}
@available(iOS 13.0, *)
extension SwiftUI.Image.GlanceImages : Swift.RawRepresentable {}
extension GlanceFramework.SessionType : Swift.Equatable {}
extension GlanceFramework.SessionType : Swift.Hashable {}
extension GlanceFramework.EventCode : Swift.Equatable {}
extension GlanceFramework.EventCode : Swift.Hashable {}
extension GlanceFramework.EventCode : Swift.RawRepresentable {}
extension GlanceFramework.EventType : Swift.Equatable {}
extension GlanceFramework.EventType : Swift.Hashable {}
extension GlanceFramework.EventType : Swift.RawRepresentable {}
extension GlanceFramework.Visibility : Swift.Equatable {}
extension GlanceFramework.Visibility : Swift.Hashable {}
extension GlanceFramework.Visibility : Swift.RawRepresentable {}
extension GlanceFramework.Message : Swift.Equatable {}
extension GlanceFramework.Message : Swift.Hashable {}
extension GlanceFramework.Message : Swift.RawRepresentable {}
extension GlanceFramework.VideoSizeMessage : Swift.Equatable {}
extension GlanceFramework.VideoSizeMessage : Swift.Hashable {}
extension GlanceFramework.VideoSizeMessage : Swift.RawRepresentable {}

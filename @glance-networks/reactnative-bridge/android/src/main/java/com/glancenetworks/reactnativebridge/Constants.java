package com.glancenetworks.reactnativebridge;

public class Constants {

    // MISCELANEOUS
    public static final String SDK_VERSION_MAP_KEY = "version";

    // SDK SETTINGS
    public static final String SESSION_KEY_MAP_KEY = "sessionkey";

    public static final String MASK_KEYBOARD_MAP_KEY = "maskKeyboard";

    public static final String VIDEO_MODE_MAP_KEY = "videoMode";

    public static final String GLANCE_TIMEOUT_MAP_KEY = "timeout";

    public static final String GLANCE_ATTEMPTS_MAP_KEY = "attempts";

    public static final String GROUP_ID_MAP_KEY = "groupID";

    public static final String VISITOR_ID_MAP_KEY = "visitorID";

    public static final String GLANCE_SERVER_MAP_KEY = "glanceServer";

    public static final String TOKEN_MAP_KEY = "token";

    public static final String NAME_MAP_KEY = "name";

    public static final String EMAIL_MAP_KEY = "email";

    public static final String PHONE_MAP_KEY = "phone";

    public static final String SKIP_DIALOG_MAP_KEY = "skipDialog";

    public static final String INVOKE_SHOW_WIDGET_MAP_KEY = "invokeShowWidget";

    public static final String SITE_MAP_KEY = "site";

    public static final String ALLOWED_CAMERAS_MAP_KEY = "allowedCameras";

    public static final String REGISTER_PRESENCE_NOTIFICATIONS_MAP_KEY = "registerNotifications";

    public static final String SCREEN_X_VALUE = "screenX";

    public static final String SCREEN_Y_VALUE = "screenY";

    // START PARAMS
    public static final String DISPLAY_PARAMS_MAP_KEY = "displayParams";

    public static final String DISPLAY_PARAMS_NAME_MAP_KEY = "displayName";

    public static final String DISPLAY_PARAMS_SCALE_MAP_KEY = "displayScale";

    public static final String DISPLAY_PARAMS_WIDTH_MAP_KEY = "displayCaptureWidth";

    public static final String DISPLAY_PARAMS_HEIGHT_MAP_KEY = "displayCaptureHeight";

    public static final String DISPLAY_PARAMS_VIDEO_MAP_KEY = "displayVideo";

    public static final String START_PARAMS_CAPTURE_ENTIRE_SCREEN_MAP_KEY = "captureEntireScreen";

    public static final String START_PARAMS_MEDIA_PROJECTION_MAP_KEY = "mediaProjectionEnabled";

    public static final String START_PARAMS_MAIN_CALL_ID_MAP_KEY = "mainCallId";

    public static final String START_PARAMS_MAX_GUESTS_MAP_KEY = "maxGuests";

    public static final String START_PARAMS_SHOW_MAP_KEY = "show";

    public static final String START_PARAMS_GUEST_INFO_FLAGS_MAP_KEY = "guestInfoFlags";

    public static final String START_PARAMS_ENCRYPT_MAP_KEY = "encrypt";

    public static final String START_PARAMS_REQUEST_RC_MAP_KEY = "requestRC";

    public static final String START_PARAMS_INSTANT_JOIN_MAP_KEY = "instantJoin";

    public static final String START_PARAMS_FORCE_TUNNEL_MAP_KEY = "forceTunnel";

    public static final String START_PARAMS_VIEWER_CLOSEABLE_MAP_KEY = "viewerCloseable";

    public static final String START_PARAMS_REPORT_ERRORS_MAP_KEY = "reportErrors";

    public static final String START_PARAMS_PERSIST_MAP_KEY = "persist";

    public static final String START_PARAMS_SHOW_TERMS_MAP_KEY = "showTerms";

    public static final String START_PARAMS_PRESENCE_START_MAP_KEY = "presenceStart";

    public static final String START_PARAMS_PAUSED_MAP_KEY = "paused";

    public static final String START_PARAMS_TERMS_URL_MAP_KEY = "termsUrl";


    // EVENTS
    public static final String GLANCE_EVENT_LISTENER_KEY = "GlanceEvent";

    public static final String GUEST_COUNT_MAP_KEY = "guestCount";

    public static final String EVENT_RECEIVED_MESSAGE_MAP_KEY = "receivedMessage";

    public static final String EVENT_RECEIVED_MESSAGE_VALUE_MAP_KEY = "receivedMessageValue";

    public static final String EVENT_WARNING_MESSAGE_MAP_KEY = "warningMsg";

    public static final String EVENT_CODE_MAP_KEY = "code";

    public static final String EVENT_TYPE_MAP_KEY = "type";

    // PRESENCE
    public static final String PRESENCE_EVENT_MAP_KEY = "presenceEvent";

    public static final String PRESENCE_DATA_MAP_KEY = "presenceDataMap";
}
